#!/usr/bin/perl -w

#     sub2text  - Clear  subtitles from all time data 
#     and leave you with the actual text

use strict;
use warnings;
my $version = "0.1";

use Getopt::Long;
Getopt::Long::Configure("pass_through","no_ignore_case");
my $help = 0;
my $showvers = 0;
my $debug = 0;
my $quiet = 0;
my $license = 0;
my $force = 0;
GetOptions("help|h",    \$help,
	   "force",	\$force,
	   "version|v", \$showvers,
	   "debug|d",   \$debug,
	   "quiet|q",   \$quiet,
	   "license|l", \$license);
	   
if ($quiet) { $debug = 0; }

if ($help) { help(); }

if ($showvers) { version(); }

if ($license) { license(); }


my $infile = shift || '';
if (!$infile) { help(); }

my $outfile = shift || '';
if (!$outfile) { 
	$outfile = $infile;
	$outfile =~ s/(\.srt)$//i;
	$outfile .= ".txt";
}

if (! -f $infile) {
	print "Input file $infile does not exist.\n";
	exit 0;
}

print "Input-file:  $infile\n" if ($debug);
print "Output-file: $outfile\n" if ($debug);

if (-f "$outfile" && !$force) {
	my $overwrite = "";
	while ( $overwrite ne "y" && $overwrite ne "n" ) {
		print "File \"$outfile\" already exists. Overwrite? <y|n> ";
		$overwrite = <STDIN>;
		$overwrite =~ s/\n//;
	}
	if ($overwrite ne "y") {
		exit 0;
	}
}

print "Trying to detect input format...\n" if ($debug);

my $format = detect_format($infile);
if (!$format) {
	print "Could not detect $infile format!\n";
	exit 0;
}

print "Converting from $format to text\n" if ($format eq "srt" && !$quiet);

open INFILE, "$infile" or die "Unable to open $infile for reading\n";
open OUTFILE, ">$outfile" or die "Unable to open $outfile for writing\n";

if ($format eq "srt") {
	conv_srt();
	print "conv_srt()\n";
}
elsif ($format eq "txt") {
	print "Input file is already txt format.\n";
}


close INFILE;
close OUTFILE;


sub conv_srt {
	my $converted = 0;
	my $failed = 0;
	while (my $line1 = <INFILE>) {
		if ($line1 !~ m/^\d/) {
			
			my $text = $line1;
			$converted++;
		
			print "  Subtitle #$converted: Text: $text\n" if ($debug);
			
			# convert line-ends
			
			write_txt($text);
		
		} else {
			if (!$converted) {
				print "  Header line: $line1 ignored\n" if ($debug);
			} else {
				$failed++;
				print "  failed to convert: $line1\n" if ($debug);
			}
		}
	}
	print "$converted subtitles written\n" if (!$quiet);
	print "$failed lines failed\n" if (!$quiet && $failed);
}



sub write_txt {
	my $text = shift;
	print OUTFILE "$text";
}


sub detect_format {
	my $file = shift;
	open INFILE, "$file" or die "Failed to open $file.\n";
	my $detected = "";
	my $i = 0;
	while (my $line = <INFILE>) {
		$line =~ s/[\n\r]*$//;
		print "  Trying line $i: $line \n" if $debug;
		
		# trying subviewer .srt format
		
		if ($line =~ m/^\d\d:\d\d:\d\d\,\d\d\d\s-->\s\d\d:\d\d:\d\d\,\d\d\d$/) {
			print "subviewer .srt format detected!\n" if ($debug);
		}
		
		
		$detected = "srt";
		return $detected;
		}
}

sub help {
print <<__HELP__;

sub2srt [options] inputfile.sub [outputfile.srt]

    Convert subrip and microdvd ".sub" subtitle files to subviewer ".srt" format
    (the format accepted by ogmmerge for multiplexing into ogm files)
    

Options:
    -h --help           Display this message.
    -v --version	Display Program version.
    -l --license	Display License information.
			 
    --force		Overwrite existing files without prompt
   
    -d --debug		Print debug information
    -q --quiet		No output


    Input file
inputfile.srt
  


[outputfile.txt]
    Output file
    Default: inputfile.txt
    
__HELP__
exit 2;
}

sub license {
print <<__VERSION__;

    srt2txt $version - Cleans subtitles .


__VERSION__
exit 2;
}

sub version {
	print "srt2txt $version\n";
	exit 2;
}
